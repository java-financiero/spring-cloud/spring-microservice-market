package es.cloudtf.pcfutbol.pcftubolmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class PcftubolMarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcftubolMarketApplication.class, args);
	}

}
